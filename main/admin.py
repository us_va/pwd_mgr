from django.contrib import admin
import datetime

from .models import AdvUser, Bb
from .utilities import send_activation_notification


def send_activation_notifications(modeladmin, request, queryset):
    for rec in queryset:
        if not rec.is_activated:
            send_activation_notification(rec)
    modeladmin.message_user(request, 'Письма с требованиями отправлены')


send_activation_notifications.short_description = 'Отправка писем с требованиями активации'


class NonactivatedFilter(admin.SimpleListFilter):
    title = 'Прошли активацию?'
    parameter_name = 'actstate'

    def lookups(self, request, model_admin):
        return (
                   ('activated', 'Прошли'),
                   ('threedays', 'Не прошли более 3 дней'),
                   ('week', 'Не прошли более недели'),
               )

    def queryset(self, request, queryset):
        val = self.value()
        if val == 'activated':
            return queryset.filter(is_active=True, is_activated=True)
        elif val == 'threedays':
            d = datetime.date.today() - datetime.timedelta(days=3)
            return queryset.filter(is_active=False, is_activated=False,
                                   date_joined__date__lt=d)
        elif val == 'week':
            d = datetime.date.today() - datetime.timedelta(weeks=1)
            return queryset.filter(is_active=False, is_activated=False,
                                   date_joined__date__lt=d)


class AdvUserAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'is_activated', 'date_joined')
    search_fields = ('username', 'email', 'first_name', 'last_name')
    list_filter = (NonactivatedFilter,)
    fields = (('username', 'password', 'email', 'lead'), ('first_name', 'last_name'),
              ('send_messages', 'is_active', 'is_activated'),
              ('is_staff', 'is_superuser'),
              'groups', 'user_permissions',
              ('last_login', 'date_joined'))
    readonly_fields = ('last_login', 'date_joined')
    actions = (send_activation_notifications,)

    def save_model(self, request, obj, form, change):
        obj.set_password(form.cleaned_data['password'])
        super(AdvUserAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(AdvUserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(lead=request.user)


admin.site.register(AdvUser, AdvUserAdmin)


class BbAdmin(admin.ModelAdmin):
    list_display = ('title', 'login', 'author', 'created_at')
    fields = (('author'), 'title', 'login', 'password', 'is_active')

    def get_queryset(self, request):
        qs = super(BbAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author__lead=request.user)


admin.site.register(Bb, BbAdmin)
