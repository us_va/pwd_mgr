from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.contrib.auth.views import LoginView, LogoutView, \
    PasswordChangeView, PasswordResetView, PasswordResetDoneView, \
    PasswordResetConfirmView, PasswordResetCompleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import UpdateView, CreateView, \
    DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.core.signing import BadSignature
from django.contrib.auth import logout
from django.contrib import messages
from django.conf import settings
from openpyxl import load_workbook

from cryptography.fernet import Fernet
import base64
import random
import tablib

from .models import AdvUser, Bb, BbCategory
from .forms import ChangeUserInfoForm, RegisterUserForm, GeneratePasswordForm, BbForm, ImportFromExcelForm, \
    BbCategoryForm, SearchForm
from .utilities import signer, import_data_from_excel


def index(request):
    bbs = Bb.objects.filter(is_active=True)
    context = {'bbs': bbs}
    return render(request, 'main/index.html', context)


def other_page(request, page):
    try:
        template = get_template('main/' + page + '.html')
    except TemplateDoesNotExist:
        return render(request, 'main/404.html')
    return HttpResponse(template.render(request=request))


class BBLoginView(LoginView):
    template_name = 'main/login.html'


@login_required
def profile(request):
    bbs = Bb.objects.filter(author=request.user.pk)
    categories = BbCategory.objects.filter(author=request.user)
    for bb in bbs:
        pas = base64.urlsafe_b64decode(bb.password)
        cipher_pass = Fernet(settings.ENCRYPT_KEY)
        decod_pass = cipher_pass.decrypt(pas).decode("ascii")
        bb.password = decod_pass
    search_form = SearchForm()
    context = {'bbs': bbs, 'categories': categories, 'search_form': search_form}
    return render(request, 'main/profile.html', context)


@login_required
def add_category(request):
    if request.method == 'POST':
        form = BbCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('main:profile')
    else:
        form = BbCategoryForm(initial={'author': request.user.pk})
    context = {'form': form}
    return render(request, 'main/profile_category_add.html', context)


@login_required
def by_category(request, pk):
    bbs = Bb.objects.filter(author=request.user, category__id=pk)
    for bb in bbs:
        pas = base64.urlsafe_b64decode(bb.password)
        cipher_pass = Fernet(settings.ENCRYPT_KEY)
        decod_pass = cipher_pass.decrypt(pas).decode("ascii")
        bb.password = decod_pass
    categories = BbCategory.objects.filter(author=request.user)
    context = {'bbs': bbs, 'categories': categories}
    return render(request, 'main/by_category.html', context)


@login_required
def search(request):
    form = SearchForm(request.POST)
    search_form = SearchForm()
    if form.is_valid():
        keyword = form.cleaned_data['name']
        bbs = Bb.objects.filter(author=request.user, title=keyword)
        for bb in bbs:
            pas = base64.urlsafe_b64decode(bb.password)
            cipher_pass = Fernet(settings.ENCRYPT_KEY)
            decod_pass = cipher_pass.decrypt(pas).decode("ascii")
            bb.password = decod_pass
        context = {'bbs': bbs, 'search_form': search_form}
        return render(request, 'main/profile.html', context)


class BBLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'main/logout.html'


class ChangeUserInfoView(SuccessMessageMixin, LoginRequiredMixin,
                                              UpdateView):
    model = AdvUser
    template_name = 'main/change_user_info.html'
    form_class = ChangeUserInfoForm
    success_url = reverse_lazy('main:profile')
    success_message = 'Данные пользователя изменены'

    def setup(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().setup(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


class BBPasswordChangeView(SuccessMessageMixin, LoginRequiredMixin,
                                                PasswordChangeView):
    template_name = 'main/password_change.html'
    success_url = reverse_lazy('main:profile')
    success_message = 'Пароль пользователя изменен'


class RegisterUserView(CreateView):
    model = AdvUser
    template_name = 'main/register_user.html'
    form_class = RegisterUserForm
    success_url = reverse_lazy('main:register_done')


class RegisterDoneView(TemplateView):
    template_name = 'main/register_done.html'


def user_activate(request, sign):
    try:
        username = signer.unsign(sign)
    except BadSignature:
        return render(request, 'main/bad_signature.html')
    user = get_object_or_404(AdvUser, username=username)
    if user.is_activated:
        template = 'main/user_is_activated.html'
    else:
        template = 'main/activation_done.html'
        user.is_active = True
        user.is_activated = True
        user.save()
    return render(request, template)


class DeleteUserView(LoginRequiredMixin, DeleteView):
    model = AdvUser
    template_name = 'main/delete_user.html'
    success_url = reverse_lazy('main:index')

    def setup(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().setup(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logout(request)
        messages.add_message(request, messages.SUCCESS, 'Пользователь удален')
        return super().post(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


class BBPasswordResetView(PasswordResetView):
    template_name = 'main/password_reset.html'
    subject_template_name = 'email/reset_letter_subject.txt'
    email_template_name = 'email/reset_letter_body.txt'
    success_url = reverse_lazy('main:password_reset_done')


class BBPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'main/password_reset_done.html'


class BBPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'main/password_confirm.html'
    success_url = reverse_lazy('main:password_reset_complete')


class BBPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'main/password_complete.html'


@login_required
def profile_bb_detail(request, pk):
    bb = get_object_or_404(Bb, pk=pk)
    pas = base64.urlsafe_b64decode(bb.password)
    cipher_pass = Fernet(settings.ENCRYPT_KEY)
    decod_pass = cipher_pass.decrypt(pas).decode("ascii")
    bb.password = decod_pass
    context = {'bb': bb}
    return render(request, 'main/profile_bb_detail.html', context)


@login_required
def profile_bb_add(request):
    if request.method == 'POST':
        form = BbForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                     'Запись добавлена')
            return redirect('main:profile')
    else:
        form = BbForm(initial={'author': request.user.pk})
    context = {'form': form}
    return render(request, 'main/profile_bb_add.html', context)


@login_required
def profile_bb_change(request, pk):
    bb = get_object_or_404(Bb, pk=pk)
    pas = base64.urlsafe_b64decode(bb.password)
    cipher_pass = Fernet(settings.ENCRYPT_KEY)
    decod_pass = cipher_pass.decrypt(pas).decode("ascii")
    bb.password = decod_pass
    if request.method == 'POST':
        form = BbForm(request.POST, request.FILES, instance=bb)
        if form.is_valid():
            bb = form.save()
            messages.add_message(request, messages.SUCCESS, 'Запись исправлена')
            return redirect('main:profile')
    else:
        form = BbForm(instance=bb)
    context = {'form': form}
    return render(request, 'main/profile_bb_change.html', context)


@login_required
def profile_bb_delete(request, pk):
    bb = get_object_or_404(Bb, pk=pk)
    if request.method == 'POST':
        bb.delete()
        messages.add_message(request, messages.SUCCESS, 'Объявление удалено')
        return redirect('main:profile')
    else:
        context = {'bb': bb}
        return render(request, 'main/profile_bb_delete.html', context)


def generate_password(request):
    chars = '+-/*!&$#?=@<>abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    if request.method == 'POST':
        form = GeneratePasswordForm(request.POST)
        if form.is_valid():
            password = ''
            for i in range(int(form.data.get('num_chars'))):
                password += random.choice(chars)
            messages.add_message(request, messages.SUCCESS,
                                 f'Пароль: {password}')
            return redirect('main:generate_password')
    else:
        form = GeneratePasswordForm()
    context = {'form': form}
    return render(request, 'main/generate_password.html', context)


class XlsxExport(object):

    FIELDS = {
        'title': 'title',
        'service_name': 'service_name',
        'login': 'login',
        'password': 'password',
    }

    def __init__(self, user):
        self.filename = f'{user.username}_passwords.xlsx'
        self.bbs = Bb.objects.filter(author=user)
        self.opts = self.bbs[0]._meta

    def get_fields(self):
        fields = list(self.FIELDS[field.column] for field in self.opts.get_fields() if field.column in self.FIELDS)
        return fields

    def prepare_data(self):
        data = []
        for obj in self.bbs:
            data_row = []
            for field in self.FIELDS.keys():
                value = getattr(obj, field)
                if field == 'password':
                    pas = base64.urlsafe_b64decode(value)
                    cipher_pass = Fernet(settings.ENCRYPT_KEY)
                    decod_pass = cipher_pass.decrypt(pas).decode("ascii")
                    value = decod_pass
                data_row.append(value)
            data.append(tuple(data_row))
        return data

    def create_xlsx(self):
        table = tablib.Dataset(headers=list(self.FIELDS.values()), title='Лист1')
        for i in self.prepare_data():
            table.append(i)
        return table


@login_required
def export_to_excel(request):
    export_obj = XlsxExport(request.user)
    export_obj.prepare_data()
    table = export_obj.create_xlsx()
    response = HttpResponse(table.export('xlsx'),
                            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8')
    response['Content-Disposition'] = 'filename={}'.format(export_obj.filename)
    return response


@login_required
def import_from_excel(request):
    if request.method == 'POST':
        wb = load_workbook(request.FILES['file'].file)
        data = import_data_from_excel(wb)
        for row in data:
            bb = Bb()
            bb.title = row['title']
            bb.service_name = row['service_name']
            bb.login = row['login']
            bb.password = row['password']
            bb.author = request.user
            bb.save()
        return redirect('main:profile')
    else:
        form = ImportFromExcelForm()
    context = {'form': form}
    return render(request, 'main/import_from_excel.html', context)
