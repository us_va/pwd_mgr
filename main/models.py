from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from cryptography.fernet import Fernet
import base64


class AdvUser(AbstractUser):
    is_activated = models.BooleanField(default=True, db_index=True,
                                       verbose_name='Прошел активацию?')
    send_messages = models.BooleanField(default=True,
                  verbose_name='Слать оповещения о новых комментариях?')
    lead = models.ForeignKey('AdvUser', on_delete=models.CASCADE, null=True, blank=True)

    def delete(self, *args, **kwargs):
        for bb in self.bb_set.all():
            bb.delete()
        super().delete(*args, **kwargs)

    class Meta(AbstractUser.Meta):
        pass


class BbCategory(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=105)
    author = models.ForeignKey(AdvUser, on_delete=models.CASCADE,
                               verbose_name='Автор')

    def __str__(self):
        return self.name


class Bb(models.Model):
    title = models.CharField(max_length=40, verbose_name='Название')
    service_name = models.URLField(null=True, blank=True)
    login = models.CharField(max_length=40, verbose_name='Логин')
    password = models.CharField(max_length=40, verbose_name='Пароль')
    author = models.ForeignKey(AdvUser, on_delete=models.CASCADE,
                               verbose_name='Автор')
    is_active = models.BooleanField(default=True, db_index=True,
                                    verbose_name='Выводить в списке?')
    created_at = models.DateTimeField(auto_now_add=True, db_index=True,
                                      verbose_name='Опубликовано')
    category = models.ForeignKey(BbCategory, on_delete=models.CASCADE,
                               verbose_name='Категория', null=True, blank=True)

    def save(self, *args, **kwargs):
        cipher_pas = Fernet(settings.ENCRYPT_KEY)
        encrypt_pass = cipher_pas.encrypt(self.password.encode('ascii'))
        encrypt_pass = base64.urlsafe_b64encode(encrypt_pass).decode("ascii")
        self.password = encrypt_pass
        super(Bb, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Пароли'
        verbose_name = 'Пароль'
        ordering = ['-created_at']
