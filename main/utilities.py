from django.template.loader import render_to_string
from django.core.signing import Signer
from openpyxl import  load_workbook

from project.settings import ALLOWED_HOSTS

signer = Signer()


def send_activation_notification(user):
    if ALLOWED_HOSTS:
        host = 'http://' + ALLOWED_HOSTS[0]
    else:
        host = 'http://localhost:8000'
    context = {'user': user, 'host': host, 'sign': signer.sign(user.username)}
    subject = render_to_string('email/activation_letter_subject.txt', context)
    body_text = render_to_string('email/activation_letter_body.txt', context)
    user.email_user(subject, body_text)


def import_data_from_excel(work_book):
    work_sheet = work_book.active
    column_names = [col[0].value for col in work_sheet.iter_cols(min_col=0, min_row=0, max_row=1)]
    table_data = []
    for row in work_sheet.iter_rows(min_row=2):
        table_data.append(
            dict(zip(column_names, tuple(str(cell.value) for cell in row))))
    return table_data
