# About
Проект написан на Python 3.10

# Функционал
Система авторизации: регистрация, авторизация, разграничение прав доступа,  
изменение учетных данных, сброс пароля.
Все это дополнено отправкой электронных писем.

Работа с паролями:
Создание, изменение, чтение, удаление

Генерация пароля на основе введенной длины пароля.

# Развертывание на локальной машине
```bash
git clone https://gitlab.com/us_va/pwd_mgr.git
cd pwd_mgr
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python manage.py migrate
```
Запуск отладочного сервера
```bash
python manage.py runserver
```
Создание суперпользователя
```bash
python manage.py createsuperuser
```
